import React, { Component } from 'react';
import './CurrentRates.css';
import FetchData from '../Data/FetchData';


class CurrentRates extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rates: []
        }
    }

    componentDidMount() {
        FetchData.getData(`/tables/C/last?format=json`)
            .then(response =>
                this.setState({ rates: response.data[0].rates })
            )
    }

    render() {
        return (
            <div>
                <div className="row justify-content-center currency">
                    <div className="row col-12">
                        <table className="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">Currency</th>
                                    <th scope="col">Code</th>
                                    <th scope="col">Bid</th>
                                    <th scope="col">Ask</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.rates.map((rates, i) =>
                                    <tr key={i}>
                                        <th scope="row">{i + 1}</th>
                                        <td>{this.state.rates[i].currency}</td>
                                        <td>{this.state.rates[i].code}</td>
                                        <td>{this.state.rates[i].bid}</td>
                                        <td>{this.state.rates[i].ask}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        );
    }
}

export default CurrentRates;
