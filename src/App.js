import React, { Component } from 'react';
import './App.css';

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import CurrentRates from './CurrentRates/CurrentRates';
import SearchRates from './SearchRates/SearchRates';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <div className='row justify-content-center'>
            <nav className="navbar navbar-light">

              <Link to='/'>
                <button className='btn btn-primary m-1'>
                  Current Rates
              </button>
              </Link>

              <Link to='/search'>
                <button className='btn btn-primary m-1'>
                  Search Rates
              </button>
              </Link>

            </nav>
          </div>
          <Route exact path='/' component={CurrentRates} />
          <Route path='/search/' component={SearchRates} />
        </div>

      </Router >
    );
  }
}

export default App;
