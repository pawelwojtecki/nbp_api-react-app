import Axios from 'axios';

class FetchData {

    API = 'http://api.nbp.pl/api/exchangerates';

    getData(path) {
        let url = `${this.API}${path}`;
        return Axios.get(url);
    }

}

export default (new FetchData());
