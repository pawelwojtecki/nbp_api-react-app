import React, { Component } from 'react';
import './SearchRates.css';
import FetchData from '../Data/FetchData';
import LineChart from '../LineChart/LineChart';

let code = 'USD';
let chartData = [(1,2), (2,3)];

class SearchRates extends Component {

    constructor(props) {
        super(props);
        this.state = {
            code: 'USD',
            count: '5',
            codes: [],
            rates: []
        }
    }

    componentDidMount() {
        FetchData.getData(`/rates/C/${this.state.code}/last/${this.state.count}`)
            .then(response => {
                this.setState({
                    ...this.state,
                    count: this.state.count,
                    code: response.data.code,
                    rates: response.data.rates
                })
            })
            .catch(error => {
                alert('Data fetching error: ', error);
            });

        FetchData.getData(`/tables/C/last?format=json`)
            .then(response =>
                this.setState({ codes: response.data[0].rates })
            )
    }

    handleClick = (event) => {
        code = this.state.code;
        event.preventDefault();
        FetchData.getData(`/rates/C/${this.state.code}/last/${this.state.count}`)
            .then(response => {
                this.setState({
                    ...this.state,
                    count: this.state.count,
                    code: response.data.code,
                    rates: response.data.rates
                })
            })
            .catch(error => {
                alert('Data fetching error: ', error);
            });
    }

    render() {
        return (
            <div className='container'>

                <div className='row justify-content-center m-2'>
                    <form onSubmit={this.handleClick} className='form-inline'>
                        <select
                            className='custom-select m-1'
                            onChange={(event) => {
                                this.setState({
                                    ...this.state,
                                    code: event.target.value
                                })
                            }}>
                            {this.state.codes.map((code, i) =>
                                <option key={i}
                                    className=''
                                    value={code.code}
                                >{code.code}</option>
                            )}
                        </select>
                        <input
                            type='number'
                            className='form-control m-1'
                            value={this.state.count}
                            onChange={(event) => {
                                this.setState({
                                    ...this.state,
                                    count: event.target.value
                                });
                            }}
                            placeholder='COUNT'
                            required />
                        <button
                            className='btn btn-outline-success btn-search m-1'
                            type='submit'>
                            SEARCH
                            </button>
                    </form>
                </div>

                <table className='table table-hover'>
                    <thead>
                        <tr>
                            <th scope='col'>{code}</th>
                            <th scope='col'>Effective Date</th>
                            <th scope='col'>Bid</th>
                            <th scope='col'>Ask</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.rates.map((rate, i) =>
                            <tr key={i}>
                                <td>{rate.no}</td>
                                <td>{rate.effectiveDate}</td>
                                <td>{rate.bid}</td>
                                <td>{rate.ask}</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default SearchRates;
